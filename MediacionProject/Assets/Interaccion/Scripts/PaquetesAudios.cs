using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]

public class PaquetesAudios 
{
    public ParametrosPaquete[] PaqueteAudiosCorrectos;
    public ParametrosPaquete[] PaqueteAudiosIncorrectos;

}
